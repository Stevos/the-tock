\chapter{Mera sličnosti}

U ovom poglavlju biće objašnjen rad sa atributima niskog nivoa i njihovom osnovnom korišćenju radi trivijalne aproksimacije sličnosti muzičkih numera na osnovu prirodnih atributa zvuka. Meru sličnosti ćemo definisati kao rastojanje između dve tačke u višedimenzionalnom prostoru. Ose ovog prostora će predstavljati statistički parametri koje ćemo izračunavati nad atributima. Svaki atribut iz skupa atributa niskog nivoa se računa na nivou okvira. Na osnovu toga koristiće s statistički parametri da bi sumirali `prosečne` vrednosti na nivou cele muzičke numere, odnosno malim skupom vrednosti aproksimirali promenu tog atributa kroz sve okvire u muzičkoj numeri.

Definisaće se pet statističkih parametara koje će se koristiti u okviru mere sličnosti. Ovo znači da će svaki izračunat atribut niskog nivoa biti predstavljen sa 5 vrednosti na nivou cele muzičke numere. Ovakav pristup je jednostavniji kada se uporedi sa kompleksnijim načinima određivanja aproksimacija muzičkih numera, kao na primer gausovih modela ili neke slične tehnike koja se zasniva na određivanju (i upoređivanju) šablona u okviru muzičke numere. Jednostavan pristup nam takođe donosi i veoma malu računsku složenost, pa samim tim je krajnji sistem dosta robusniji i sposobniji da se brže i lakše izbori sa većim količinama ulaznih podataka. Ali isto tako je podložniji greškama usled međusobno permutovanih delova muzičke numere, pošto kao što je već rečeno, ocene gledaju pesmu u celosti a ne po segmentima. U okviru ovog rada definisaćemo statističke parametre za svaki atribut pojedinačno na nivou cele muzičke numere, odnosno u obzir se uzimaju svi okviri odjednom umesto grupisanja okvira u posebne grupe koje će kasnije učestvovati u računanju mere sličnosti. 

Na kraju će se na osnovu definisanog test skupa odrediti koje ocene najviše utiču na atribute visokog nivoa koji će se definisati i odrediti na osnovu subjektivnog osećaja. Iako ovaj pristup sa sobom nosi i određene nedostatke, on predstavlja dobru osnovu za dalji rad i unapređivanje mera sličnosti muzičkih numera.

Prilikom računanja atributa, kao i statističkih parametara koristićemo jedan vid normalizacije. Prilikom čitanja vrednosti uzoraka iz muzičke datoteke koriste se \verb|int16_t| vrednosti, odnosno označene celobrojne vrednosti iz intervala $[-32768, 32767]$. U toku učitavanja ove vrednosti se normalizuju na interval $[-1, 1]$ radi, uslovno rečeno, lepših vrednosti atributa.

\section{Statistički parametri}

Statistički parametri se koriste da se brojevnom vrednošću bliže odrede vrednosti skupa podataka. U ovom slučaju je to odličan alat za okvirnu aproksimaciju sličnosti muzičkih numera. Ovde se koristi pet statističkih parametara odnosno ocena, da bi se dobila približna slika o fluktuaciji vrednosti atributa kroz okvire. 

\begin{itemize}

\item \textbf{Srednja vrednost} je osnovna statistička ocena. Informaciju koju pruža je prosečna vrednost atributa na nivou cele muzičke numere. Srednja vrednost se može iskoristiti da grubo aproksimira vrednost atributa i poslužiti za osnovno upoređivanje atributa između dve muzičke numere

\begin{equation}
SrednjaVrednost = \frac{\sum_{i = 0}^{N-1} s_{i}}{N}
\end{equation}


\item \textbf{Varijansa} predstavlja očekivano matematičko očekivanje odstupanja slučajne promenljive od srednje vrednosti. Varijansa nikada ne sme biti negativna vrednost, to ukazuje na grešku u računu.

\begin{equation}
Varijansa = \frac{\sum_{i = 0}^{N-1} (s_{i} - SrednjaVrednost)^{2}}{N}
\end{equation}


\item \textbf{Standardna devijacija}, u statistici, predstavlja apsolutnu meru disperzije skupa, odnosno koliko u proseku elementi odstupaju od aritmetičke sredine skupa. Standardna devijacija i varijansa su u direktnom odnosu, pošto je varijansa kvadratni stepen standardne devijacije. U okviru implementiranog sistema i aproksimacije sličnosti, je zadržana zajedno sa varijansom kao statistički parametar zbog uticaja na izračunavanje ostalih statističkih parametera. 

\begin{equation}
StdDev = \sqrt{Varijansa}
\end{equation}


\item \textbf{Koeficijent asimetrije (eng. \textit{Skewness})} je statistička mera koja pokazuje koliko se vrednosti skupa slučajne promenljive razlikuju u odnosu na normalnu raspodelu, odnosno definiše asimetriju skupa sa normalnom raspodelom. 
\begin{equation}
Koeficijent_asimetrije = \frac{\sum_{i = 0}^{N-1} (s_{i} - SrednjaVrednost)^{3}}{N} \cdot StdDev^{3}
\end{equation}


\item \textbf{Koeficijent spljoštenosti (eng. \textit{Kurtosis})} mera koliko su vrednosti skupa spljoštene u odnosnu na normalnu raspodelu. Aproksimira se vrh krive koja predstavlja vrednosti iz skupa slučajne veličine. 
\begin{equation}
Koeficijent_spljoštenosti = \frac{\sum_{i = 0}^{N-1} (s_{i} - SrednjaVrednost)^{4}}{N} \cdot StdDev^{4}
\end{equation}


\end{itemize}

\section{Mera sličnosti}

Mera sličnosti će se definisati kao rastojanje u više dimenionalnom prostoru, odnosno posmatraće se sve dobijene vrednosti kao osu u višedimenzionalnom prostoru. Svaka muzička numera će na osnovu vrednosti dobijenih izračunavanjem atributa i statističkih parametara imati mesto u tom prostoru. Meru sličnosti definišemo kao rastojanje između dve tačke u tom prostoru. U ovom slučaju će se koristiti Euklidsko rastojanje koje se definiše na sledeći način:
\begin{equation}
EuclideanDist = \sqrt{\sum_{i = 1}^{n} (A_i - B_i)^2}
\end{equation}

Gde su $A_i, B_i, i \in [0, n-1]$ izračunate vrednosti statističkih parametara za svaki od atributa niskog nivoa za dve muzičke numere $A$ i $B$. $n$ predstavlja ukupan broj vrednosti, po muzičkoj numeri, koje učestvuju u izračunavanju mere sličnosti ($brojAtributa \cdot brojStatistckihParametara$).

Ovakav način aproksimacije sličnosti potencijalno zanemaruje odnos intervala u muzičkim numerama, odnosno vrlo je verovatno da ista muzička numera sa izmenjenim redosledom dobije isto mesto u višedimenzionalnom prostoru u okviru kog određujemo sličnost dve muzičke numere. Može se diskutovati da li je muzička numera sa permutovanim intervalima ista muzička numera? Ovde se misli na prvenstveno drugačiji redosled istih intervala u muzičkoj numeri, na primer, okrenuti strofa i refren, ili drastičniji slučaj, kada se ispremeštaju mnogo manji vremenski intervali na slučajan način. Radi lakše implementacije i otpornosti samog pristupa na različitost u dužini trajanja muzičkih numera, u ovom radu je korišćen pristup gde je muzička numera posmatrana u celosti. I pored očiglednih nedostataka, mera sličnosti nam pruža subjektivno primetan poredak i samim tim zadovoljava početni uslov gde se redosled muzičkih numera određuje na osnovu samog audio signala i odgovara subjektivnom osećaju korisnika (čoveka) na muzičke numere.

Treba formuilsati način na koji će se mera sličnosti upotrebiti u svrhu što boljeg generisanja reprodukcione liste. Prisutnost semantičkog raskoraka, dosta otežava formiranje dobrog poretka muzičkih numera. Korišćenje dobijenih vrednosti pruža objektivnu aproksimaciju sličnosti audio numera, kako je ideja da se mera sličnosti iskoristi u svrhu pravljenja odgovarajuće liste, potrebno je dodatno analizirati dobijene podatke i odrediti vezu, ukoliko postoji, između karakteristika muzičke numere koje čovek koristi da bi opisao pesmu. Ovakvim pristupom može se približno odrediti koje ocene direktno utiču na karakteristike od interesa. Mera sličnosti u osnovi predstavlja relaciju između dve muzičke numere. Pristup apsolutnog određivanja ocena na nivou cele pesme nam omogućava da dobijene ocene iskoristimo samostalnu aproksimaciju muzičke numere. Ovo efektivno znači da, ukoliko se izoluje određena ocena, možemo sortirati odnosno kreirati listu uređenu prema zadatom kriterijumu. Osim toga, pruža dobru osnovu za korišćenje raznih metoda mašinskog učenja za klasterovanje i upoređivanje i izolovanje sličnih pesama, što nije fokus ovog rada, ali predstavlja dobru osnovu za dalje istraživanje.
Da bi definisali takav pristup koristićemo test skup nasumično izabranih muzičkih numera. Test skup je formiran tako da se biraju 3 (ili više) numera iz istog žanra, odnosno subjektivnog osećaja o sličnosti. Ovakav pristup nam pruža dobru sliku o tome koliko je mera sličnosti efikasna i koliko je sličnost dobro određena. 

\section{Test skup}

Izabrani test skup čini 21 muzička numera, odabrane prema subjektivnom osećaju. Muzičke numere su preuzete sa sajta YouTube, odnosno samo audio tok podataka sa video klipova. Treba napomenuti da muzička numera generisana na ovaj način može da sadrži i neke nepotrebne elemente, kao što su pozadinski razgovor u toku spota ili tišinu tokom odjavne scene.

\captionof{table}{Test skup} 
\begin{tabular}{|p{0.5cm}| p{3cm} | p{2.5cm} | p{2cm} | p{2cm} | p{2cm} |}
\hline
ID & Izvođač & Muzička numera & Žanr & Kategorija 1 & Kategorija 2 \\
\hline
 1 & Lady Gaga & Bad Romance & Techno & brza & jaka\\
\hline
 2 & Foreigner & I want to know what love is & RnR & spora & dinamična \\
\hline
 3 & Robbie Wiliams and  \newline
Nicole Kidman & Something stupid & Pop-Bolero-\newline
Jazz & melodična & romantična \\
\hline
 4 & Luis Fonsi ft. \newline Daddy Yankee & Despacito & Pop-regeton & brza & latino \\
\hline
 5 & Ricky Martin & Maria & Latino pop & energična & latino\\
\hline
 6 & Coldplay & Fix you & Alternativni Rock & spora & smirujuća\\
\hline
 7 & Eminem & Rap God & Hip-hop & brza & energična\\
\hline
 8 & Beogradski sindikat & Sistem te laže & Hip-hop & agresivna & energična \\
\hline
 9 & Joe Cocker & You can leave hat on & Rock & srednji tempo & vesela\\
\hline
 10 & Ceca & Znam & Narodna & ritmična & vesela \\
\hline
 11 & Orthodox Celts & Far away & Irski folk & brza & energična\\
\hline
 12 & Đorđe Balašević & Lepa protina kći & Šansona & spora & melanholična \\
\hline
 13 & Šaban Šaulić & Samo za nju & Narodna muzika & balada & srednji tempo \\
\hline
 14 & Queen & Dont stop me now & Classic RockNRoll & brza & ritmična \\
\hline
 15 & Guns N Roses & Welcome to the jungle & RockNRoll & brza & jaka \\
\hline
 16 & Zdravko Čolić & Ti si mi u krvi & Pop & balada & spora \\
\hline
 17 & Dubioza Kolektiv & Rijaliti & Rap-rock & energetična & stimulativna \\
\hline
 18 & Johannes Brahms & Hungarian dance & Klasična muzika & dinamična & jaka \\
\hline
 19 & Ludwig van Beethoven & Za Elizu & Klasična muzika & nežna & ceremonijalna \\
\hline
 20 & Rimsky-Korsakov & Bumbarov let & Klasična muzika & brza & energična \\
\hline
 21 & Miroslav Ilić & Božanstvena ženo & narodna & melodična & nežna \\
\hline
\end{tabular}

 Na sam sistem takve stvari ne bi trebalo da utiču mnogo, ali su svakako prisutne u svakodnevnoj upotrebi, stoga postojanje nesavršenosti u muzičkim numerama ne utiče značajno na dalju analizu atributa. U tabeli 4.1 je opisan test skup. Test skup sadrži 21 muzičku numeru. Svaka numera je opisana izvođačem, nazivom numere, žanrom kojem pripada, kao i sa dva atributa visokog nivoa. Inicijalno testiranje sistema nad test skupom koji sadrži muzičke numere različitog žanra i muzičke numere sa izraženo različitim prirodnim karakteristikama će dati okvirne vrednosti i intervale u kojima se atributi kreću. Ovaj korak je izuzetno bitan u analizi dobijenog sistema pošto, kao što je već rečeno, koristićemo Euklidsko rastojanje kao aproksimativnu meru sličnosti dve muzičke numere. Kako svaki atribut učestvuje u računanju sličnosti, ako se atributi kreću u različitim intervalima vrednosti, na primer $a=[0,1]$ i $b=[1000,5000]$ atribut $b$ bi mnogo više uticao na rezultat rastojanja, od atributa $a$. Ovakva situacija je nepoželjna, bar ne u nekontrolisanim uslovima, jer atribut $a$ možda nosi mnogo bitnije informacije od atributa $b$. 

Za svaku muzičku numeru su dodata dva atributa visokog nivoa. Vrednosti atributa visokog nivoa su izvedena na osnovu subjektivnog osećaja na skupu ljudi. Kategorije nisu nužno disjunktne, odnosno vrednosti iz jedne i druge kategorije se mogu poklapati. Scenario gde se vrednosti dve kategorije poklapaju, to jest, nose istu informaciju, je vrlo česta pojava. Muzička numera, kao entitet, koji se vrlo subjektivno doživljava, najizraženija osobina pesme ostavlja jak  utisak na slušalaca i utiče na doživljaj same muzičke numere. Ovo dovodi do toga da se slušalac opisuje pesmu imajući u vidu dominantnu karakteristiku pesme, često koristeći sinonime za istu karakteristiku, na primer, pesma može biti brza i energična, što se može vrlo lako reći za pesmu koja ima brz tempo. Takođe, to nužno ne mora biti tačno, jer na doživljaj muzičke numere može da utiče i poruka pesme. Ovde se prvenstveno misli na reči pesme i poruke koju muzička numera nosi na "pesničkom" nivou koji nema direktnu vezu sa samim karakteristikama zvuka, odnosno "pozadinske" muzike. Muzička numera može da ima spor ritam i da ima nežne tonove a da ima takav tekst da korisnika može poneti poruka koju tekst nosi i pesmu protumačiti kao energičnu. 

Prilikom analize sistema i daljeg modeliranja je bitno imatu ispravnu predstavu o podacima. Kako je muzika veoma subjektivan pojam i zavisi od mnogo različitih faktora, od kojih neke trenutno ne možemo izmeriti pomoću računara. Aproksimacija sličnosti će se bazirati na karakteristikama zvuka, koje ne moraju nužno biti dovoljne za tačnu aproksimaciju.

\subsection{Predstavljanje rezultata}

Kao što je već pomenuto sami rezultati nisu dovoljni, to jest, potrebno je izvršiti analizu i po potrebi normalizovanje vrednosti. Normalizacija je neophodna da bi svaki atribut, odnosno ocena, podjednako učestvovala u inicijalnoj aproksimaciji vrednosti. 

\captionof{table}{Vrednosti atributa bez normalizacije ulaznih vrednosti} 
\begin{tabular}{|p{4cm}| p{2cm} | p{2cm} | p{2cm} | p{2cm} |}
\hline
Atribut & Srednja vrednost & Varijansa & Koeficijent asimetrije & Koeficijent spljoštenosti \\
\hline
Broj promena znaka amplitude & -0.99770 & 0.00442 & 29.437 & 875.606  \\
\hline
Autokorelacija prvog reda & 0.49110 & 0.26131 & -0.08103 & 1.28028 \\
\hline
Srednjekvadratna energija & 0.69864 & 0.09966 & -0.69755 & 4.86035 \\
\hline
Najveća amplituda & -0.99753 & 0.00491 & 28.4429 & 810.131 \\
\hline
Spektralni centroid & -0.71259 & 0.03957 & 0.06025 & 1.45597 \\
\hline
Linearna regresija & -0.60128 & 0.1633 & 0.25590 & 2.08424 \\
\hline
Glatkoća zvuka & -0.99917 & 0.00094 & 44.0803 & 2224.82 \\
\hline
Spektralni raspon & -0.67355 & 0.09124 & 0.02554 & 1.0868 \\
\hline
Spektralna asimetrija & -0.55007 & 0.20413 & 0.01306 & 1.01433 \\
\hline
\end{tabular}

\pagebreak

\captionof{table}{Vrednosti atributa sa normalizacijom ulaznih vrednosti} 
\begin{tabular}{|p{4cm}| p{2cm} | p{2cm} | p{2cm} | p{2cm} |}
\hline
Atribut & Srednja vrednost & Varijansa & Koeficijent asimetrije & Koeficijent spljoštenosti \\
\hline
Broj promena znaka amplitude & 2.243e-06 & 4.221e-09 & 29.437 & 875.606 \\
\hline
Autokorelacija prvog reda & 0.74482 & 0.06520 & -0.08102 & 1.28003 \\
\hline
Srednjekvadratna energija & 0.84930 & 0.02491 & -0.69735 & 4.86055 \\
\hline
Najveća amplituda & 0.00123 & 0.00122 & 28.4429 & 810.131 \\
\hline
Spektralni centroid & 178.228 & 15217.2 & 0.05988 & 1.45616 \\
\hline
Linearna regresija & -1.31383 & 0.10993 & 0.25528 & 2.0844 \\
\hline
Glatkoća zvuka & 0.02252 & 0.69573 & 44.078 & 2224.66 \\
\hline
Spektralni raspon & 571.042 & 279308 & 0.02623 & 1.08698 \\
\hline
Spektralna asimetrija & 1213.71 & 1.587e+06 & 0.01287 & 1.01432 \\\hline
\end{tabular}


Kao uzorak u uzeta je muzička numera \textit{Despacito}, kao najpregledaniji video snimak na sajtu \textit{YouTube}.

Tabele 4.1 i 4.2 sadrže vrednosti dobijene bez normalizacije ulaznih  vrednosti i sa. Prva tabela predstavlja statističke ocene sa vrednostima gde su atributi normalizovani na interval $[-1,1]$ u odnosu sami na sebe, dok druga tabela predstavlja vrednosti bez normalizacije. Lako se uočava da su vrednosti u prvoj tabeli daleko lepše i uslovno rečeno stabilnije, tako da će se zadržati ovakav vid normalizacije. Moguće je diskutovati o gubitku određenog dela informacija normalizacijom podataka na ovaj način, ali kako se podaci računaju nad okvirima određene dužine, i uzimajući u obzir njihovo vremensko trajanje, mnogo više informacija se dobija analizom fluktuacije njihovih vrednosti, pa samim tim normalizacija podataka u odnosu na same sebe, ne remeti šablon fluktuacije i ne utiče značajno na statističke ocene po atributima.

Nakon inicijalnog pokretanja sistema dobijaju se sledeći rezultati:

\begin{lstlisting}[language=bash]
Luis Fonsi ft. Daddy Yankee - Despacito.mp3 :                  0
Orthodox Celts - Far Away.mp3 :                              903.523
Robbie Williams and Nicole Kidman - Something Stupid.mp3 :  2525.16
Bethoven - Fur Elise.mp3 :                                  5603.6
Johannes Brahms - Hungarian Dance No. 5.mp3 :               7090.75
Dubioza kolektiv - Rijaliti.mp3 :                           7282.75
Djordje Balasevic - Lepa protina kci.mp3 :                  9863.46
Ricky Martin - Maria.mp3 :                                 11089.7
Lady Gaga - Bad Romance.mp3 :                              11224
Miroslav Ilic - Bozanstvena zeno.mp3 :                     11737.4
Saban Saulic - Samo za nju.mp3 :                           11774.5
Beogradski Sindikat - Sistem Te Laze.mp3 :                 11912.2
Rimsky-Korsakov - Flight Of The Bumblebee.mp3 :            12852.5
Queen - Dont Stop Me Now.mp3 :                             13372.5
Ceca - Znam.mp3 :                                          14424.9
Zdravko Colic - Ti si mi u krvi.mp3 :                      15095.1
Coldplay - Fix You.mp3 :                                   16371.5
Guns N Roses - Welcome To The Jungle.mp3 :                 16734.5
Foreigner - I Want To Know What Love Is.mp3 :              17909.6
Joe Cocker - You Can Leave Your Hat On.mp3 :               20539.4
Eminem - Rap God.mp3 :                                     22584.6
\end{lstlisting}

Na osnovu prethodne tabele ocena baziranih na referentnoj pesmi \textit{Despacito} vidi velika razlika u veličinama ocena, odnosno pojedine ocene se kreću u intervalu od $[-1,1]$ dok recimo spektralna asimetrija dostiže vrednosti preko hiljadu. Pošto se za meru rastojanja koristi Euklidsko rastojanje, veća vrednost pojedinih ocena prirodno pravi veći uticaj na samo rastojanje, pa samim tim i na dobijene rezultate. Potrebno je naglasiti da je računato rastojanje od referentne muzičke numere \textit{Despacito} sa svakom muzičkom numerom ponaosob, odnosno broj predstavlja aproksimaciju sličnosti sa referentnom pesmom, uključujući nju samu.

\section{Normalizacija statističkih ocena atributa}

Normalizacija statističkih parametera baziranih na osnovu atributa i raspodeli njihovih vrednosti po okvirima pesme, na isti način kao što je normalizovana vrednost samih atributa nije optimalna, odnosno količina informacija koja se izgubi time nanosi više štete nego koristi. Pošto u ovom slučaju statistička ocena atributa je singularna vrednost, njena normalizacija u odnosu na samu sebe bi uvek bila maksimalna vrednost tog parametra. Normalizacija ocena pesme je izuzetno potrebna zbog rezultata predstavljenih u prethodnoj sekciji. Najmanje izmerene vrednosti ocena se nalaze u sledećoj tabeli. Svaka ćelija sadrži respektivno  minimalnu i maksimalnu vrednost za zadati test skup. 

\captionof{table}{Najmanja i najveća vrednost atributa po okviru} 
\noindent \begin{tabular}{|p{4cm}| p{2cm} | p{2cm} | p{2cm} | p{2cm} |}
\hline
Atribut & Srednja vrednost & Varijansa & Koeficijent asimetrije & Koeficijent spljoštenosti \\
\hline
Broj promena znaka amplitude & -0.9998   0.0000 & 0.0000   0.0132 & 0.0000   104.156 & 0.0000   10849.6 \\
\hline
Autokorelacija prvog reda & 0.4831   0.4922 & 0.2597   0.2717 & -0.1552   -0.0684 &1.2382   1.5244\\
\hline
Srednjekvadratna energija & 0.6895   0.6999 & 0.0974   0.1144 & -1.1584   -0.6067 & 4.4033   6.9237 \\
\hline
Najveća amplituda & -0.9998   0.0000 & 0.0000   0.0132 & 0.0000   104.156 & 0.0000   10849.6\\
\hline
Spektralni centroid & -0.7142   0.1783 & 0.03954   0.6678 & 0.0059   0.0934 & 1.0081   1.7051\\
\hline
Linearna regresija & -0.6036   -0.5942 & 0.1614   0.1764 & 0.2163   0.4641 & 1.9260   2.8799\\
\hline
Glatkoća zvuka & -0.9998   0.0000 & 0.0000   0.0016 & 0.0000   104.156 & 0.0000   10849.6\\
\hline
Spektralni raspon & -0.9998   0.0251 & 0.0002   0.9206 & 0.0178   126.272 & 1.0012   15954.7\\
\hline
Spektralna asimetrija & -0.5501   0.9998 & 0.0002   0.9988 & -126.311   0.0317 & 1.0007   15961.5\\
\hline
\end{tabular}

Gde se vrednosti svi atributa kreću u okvirnom intervalu $[-126, 15000]$, što se iz može videti u izlaznim podacima aplikacije. Problem sa ovakvim intervalom je što ocene nisu podjednako dobro raspoređene po tom intervalu. Stoga će se primeniti pristup normalizacije ocena po skupu. Pošto ćemo reprodukcionu listu određivati na osnovu sličnosti svake pojedinačne muzičke numere sa referentnom numerom, na datom skupu određuje se najmanja i najveća vrednost po atributu i normalizovati ocene svake muzičke numere ponaosob. 
Ovakav pristup može biti osetljiv na promene u skupu nad kojim vršimo procenu sličnosti, međutim za potrebe ovog rada pretpostavićemo da su sve muzičke numere sa kvalitetom snimka koji ne odstupa preterano od prosečnog, odnosno nema dugih i izraženih smetnji koje bi drastično uticale na rezultate.

Rezultati ocena nakon primenjene normalizacije ocena, na referentnoj pesmi su:

\captionof{table}{Vrednosti atributa nakon normalizacije ulaznih vrednosti i normalizacije vrednosti atributa} 
\begin{tabular}{|p{4cm}| p{2cm} | p{2cm} | p{2cm} | p{2cm} |}
\hline
Atribut & Srednja vrednost & Varijansa & Koeficijent asimetrije & Koeficijent spljoštenosti \\
\hline
Broj promena znaka amplitude & -0.99577 & -0.33081 & -0.43475 & -0.83859 \\
\hline
Autokorelacija prvog reda & 0.73987 & -0.73537 & 0.71063 & -0.70649 \\
\hline
Srednjekvadratna energija & 0.73937 & -0.73692 & 0.67070 & -0.63734 \\
\hline
Najveća amplituda & -0.99544 & -0.25636 & -0.45384 & -0.8506 \\
\hline
Spektralni centroid & -0.99635 & -0.99988 & 0.24141 & 0.28506 \\
\hline
Linearna regresija & -0.49246 & -0.75229 & -0.68107 & -0.66838 \\
\hline
Glatkoća zvuka & -0.99871 & 0.14633 & -0.15357 & -0.58988 \\
\hline
Spektralni raspon & -0.3633 & -0.80227 & -0.99987 & -0.99998 \\
\hline
Spektralna asimetrija & -1 & -0.59164 & 0.99970 & -0.99999 \\
\hline
\end{tabular}

Pokretanje sistema sa normalizovanim ocenama daje nešto bolje rezultate, sudeći bar po subjektivnom osećaju.

\begin{lstlisting}[language=bash]
Luis Fonsi ft. Daddy Yankee - Despacito.mp3 :               0
Ricky Martin - Maria.mp3 :                                  3.34701
Coldplay - Fix You.mp3 :                                    3.83882
Dubioza kolektiv - Rijaliti.mp3 :                           3.93188
Lady Gaga - Bad Romance.mp3 :                               4.07851
Zdravko Colic - Ti si mi u krvi.mp3 :                       4.32923
Foreigner - I Want To Know What Love Is.mp3 :               4.33964
Johannes Brahms - Hungarian Dance No. 5.mp3 :               4.64319
Guns N Roses - Welcome To The Jungle.mp3 :                  4.68333
Saban Saulic - Samo za nju.mp3 :                            4.79902
Eminem - Rap God.mp3 :                                      4.98876
Beogradski Sindikat - Sistem Te Laze.mp3 :                  5.02376
Orthodox Celts - Far Away.mp3 :                             5.30745
Miroslav Ilic - Bozanstvena zeno.mp3 :                      5.57593
Djordje Balasevic - Lepa protina kci.mp3 :                  6.14696
Queen - Dont Stop Me Now.mp3 :                              6.27649
Ceca - Znam.mp3 :                                           6.62068
Rimsky-Korsakov - Flight Of The Bumblebee.mp3 :             6.80863
Joe Cocker - You Can Leave Your Hat On.mp3 :                6.81283
Bethoven - Fur Elise.mp3 :                                  7.59254
Robbie Williams and Nicole Kidman - Something Stupid.mp3 :  7.77325
\end{lstlisting}

Nakon pažljivog slušanja test skupa uočavaju se znatni skokovi i neskladnosti između uzastopne dve muzičke numere na dobijenoj reprodukcionoj listi. Razlog zbog ovoga leži u načinu određivanja sličnosti dve muzičke numere. Prva i najbliža pesma će uvek biti jedinstveno određena, ono što predstavlja problem je taj što je lista generisana na osnovu distanci od početne referentne pesme. Ako se problem svede u jednu dimenziju i pesme posmatraju kao vrednosti na pravi, najbliža muzička numera može biti sa desne strane referentne muzičke numere dok druga najbliža može biti sa leve strane. Kako reprodukcija ide redom, već kod druge muzičke numere dobijamo nekonzistentne i nedovoljno pouzdane podatke na prvi pogled. Kako reprodukcija odmiče, problem je sve očigledniji i svodi se na slučajan izbor.

Da bi se izbegla pomenuta situacija, potrebno je izmeniti pristup formiranja reprodukcione liste. Umesto da određujemo celu listu prema sličnosti referentnoj muzičkoj numeri, svaku sledeću muzičku numeru određujemo na osnovu sličnosti trenutnoj muzičkoj numeri, izuzimajući sve već reprodukovane muzičke numere. Ovakav pristup obezbeđuje pouzdaniji izbor sledeće muzičke numere i gladak prelaz između žanrova. Sa druge strane, u nekom trenutku se može pojaviti jak skok između sličnosti dve uzastopne numere, što može predstavljati problem. Skok je neminovan ukoliko sličnost odvede na jednu stranu, kako u svakom koraku eliminišemo po jednu muzičku numeru iz skupa nad kojim se izračunava sličnost, dolazimo do momenta gde smo otišli previše u jednu stranu i prva najbliža muzička numera se nalazi na drugom kraju hiperravni.

\begin{lstlisting}[language=bash]
Luis Fonsi ft. Daddy Yankee - Despacito.mp3 :               0
Ricky Martin - Maria.mp3 :                                  3.34701
Coldplay - Fix You.mp3 :                                    1.18222
Foreigner - I Want To Know What Love Is.mp3 :               1.59933
Guns N Roses - Welcome To The Jungle.mp3 :                  2.22068
Zdravko Colic - Ti si mi u krvi.mp3 :                       1.84687
Lady Gaga - Bad Romance.mp3 :                               1.65483
Eminem - Rap God.mp3 :                                      2.07
Johannes Brahms - Hungarian Dance No. 5.mp3 :               4.3616
Miroslav Ilic - Bozanstvena zeno.mp3 :                      2.83593
Queen - Dont Stop Me Now.mp3 :                              1.33646
Ceca - Znam.mp3 :                                           3.216
Joe Cocker - You Can Leave Your Hat On.mp3 :                1.6826
Saban Saulic - Samo za nju.mp3 :                            5.14189
Dubioza kolektiv - Rijaliti.mp3 :                           1.27078
Beogradski Sindikat - Sistem Te Laze.mp3 :                  1.55886
Djordje Balasevic - Lepa protina kci.mp3 :                  3.61917
Rimsky-Korsakov - Flight Of The Bumblebee.mp3 :             3.61997
Bethoven - Fur Elise.mp3 :                                  2.1165
Robbie Williams and Nicole Kidman - Something Stupid.mp3 :  5.61598
\end{lstlisting}

Rezultati nakon promene pristupa prilikom generisanja reprodukcione liste pokazuju mnogo bolje rezultate nego što je to bilo sa prvobitnim računanje rastojanja u odnosu na referentnu pesmu. Zbog test skupa, koji je prilično raznolik, postoje neznatni i primetni skokovi tokom kreiranja reprodukcione liste. Iz priloženog se vidi da se prag sličnosti nalazi negde u intervalu $[2, 3.5]$, odnosno vrednost razdaljine koja više nego očigledno pravi razliku između dve muzičke numere. Treba napomenuti da je mera sličnosti u odnosu sa kojom se upoređuju rezultati, strogo subjektivna i zasniva se na sluhu i interpretaciji muzičkih numera od strane autora ovog rada.

\section{Ocena mere sličnosti}

Prethodni rezultati se tiču generisanja reprodukcione liste. Međutim u ovom koraku je potrebno pozabaviti se i samom ocenom mere sličnosti. Pošto je sama mera sličnosti izuzetno subjektivan pojam, odnosno dosta zavisi od slušalaca do slušalaca, mera sličnosti je posmatrana na nekoliko manjih grupa, od po pet muzičkih numera, gde su dve muzičke numere očigledno slične jedna drugoj i dosta različite u odnosu na ostale tri. U velikoj meri izražena sličnost dve pesme, odnosno  različitost od ostalih, obezbeđuje nepristrasnost prilikom određivanja sličnosti. Razlog za to je već pomenuto subjektivno shvatanje muzičkih numera od strane čoveka. 

Mera sličnosti je testirana na tri grupe od po pet pesama, kao što je već bilo reči. Rezultati dobijeni prilikom ovog testiranja su sledeći:

Grupa 1:
\begin{lstlisting}[language=bash]
Chosen: Bryan Adams - Have You Ever Really Loved A Woman_.mp3
Lady Gaga - Bad Romance.mp3                                    8.40734
Orthodox Celts - Far Away.mp3                                  9.64667
Djani - Ja slobodan ona neverna - (Audio 2008).mp3             9.85885
Toni Braxton - Un-Break My Heart (Video Version).mp3           6.89885
Bryan Adams - Have You Ever Really Loved A Woman_.mp3          0
\end{lstlisting}

Grupa 2:
\begin{lstlisting}[language=bash]
Chosen: Bolero - Maurice Ravel.mp3
Johann Strauss II - The Blue Danube Waltz.mp3                  7.5346
Frank Sinatra Fly Me To The Moon.mp3                           8.9684
Bolero - Maurice Ravel.mp3                                     0
Beogradski Sindikat - Sistem Te Laze.mp3                       9.20094
Foo Fighters - The Pretender.mp3                               8.30397
\end{lstlisting}

Grupa 3:
\begin{lstlisting}[language=bash]
Chosen: Guns N Roses - Welcome To The Jungle.mp3
Djordje Balasevic - Ne lomite mi bagrenje.mp3                  7.40437
Ceca - Lepi grome moj.mp3                                      8.81831
Metallica - Enter Sandman.mp3                                  2.5572
Frank Sinatra - The Way You Look Tonight.mp3                   8.6312
Guns N Roses - Welcome To The Jungle.mp3                       0
\end{lstlisting}

Sličnost između muzičkih numera grupisanih u istu grupu je očigledna. Na prvi pogled, mera sličnosti se pokazala odgovarajućom. Prilikom malo dublje analize rezultata, iako su rezultati zadovoljavajući, primećuje se razlika  u brojevima koja nas navodi da mera sličnosti može biti nestabilna. Ovakav rezultat se dobija na manjem skupu analiziranih pesama jer je pristup normalizacije atributa takav da uzima u obzir sve vrednosti atributa iz celog skupa. Iako nije idealan, ovakav pristup će biti dovoljan za svrhu ovog rada i prikaza aproksimacije mere sličnosti u automatskom određivanju reprodukcionih lista.

U okviru grupe 1, dve najsličnije pesme su od izvođača Bryan Adams i Toni Braxton, u grupi 2, Johann Strauss II i Maurice Ravel, a u grupi 3, Guns N Roses i Metallica. U sva tri slučaja sistem je pokazao zadovoljavajući rezultat. Ovo međutim, ne mora da znači da će sistem uvek biti tačan, šta više, vrlo je moguće da će za različite muzičke numere, gde postoje dve očigledno slične, dati pogrešne rezultate, usled već pomenutog, ne baš najbolje izabranog načina za normalizaciju atributa.

Za potrebe ovog rada, međutim, ovakav pristup se pokazao zadovoljavajućim. Iz priloženog se vidi da se mera sličnosti ponaša dovoljno stabilno i na manjim grupama muzičkih numera. Prilikom rasta skupa, mera sličnosti će biti stabilnija jer će se normalizacija prilagođavati dominatnijem  delu muzičkih numera, dok će se način izračunavanja sličnosti pobrinuti da ne bude presudan faktor. Ovo u principu znači da će na rezultate, osim korisnika, uticati i sam skup podataka u celosti. Ovakav pristup, teoretski, može doneti i još jedan nevidljiv atribut prilikom izračunavanja mere sličnosti, jer na osnovu izabranog skupa pesama od strane korisnika, mogu se donekle odrediti i preference korisnika, u ovom slučaju, u određenoj meri, izraženi prilikom normalizacije vrednosti atributa.

\section{Implementacioni detalji}

Implementacija ponašanja navedenog u okviru ovog poglavlja se nalazi u sledećim komponentama \verb|Playlist|, \verb|Distances| i \verb|feature_group|. \verb|Playlist| implementira logiku iza formiranja reprodukcionih lista. Predstavlja ulaznu tačku u sistem i kao takva rukovodi celim tokom za obradu pesme i ekstraktovanje atributa. Osnovne funkcionalnosti same komponente su implementirane u potpunosti, dok dalja istraživanja otvaraju mogućnost razvoja novih funkcionalnosti. \verb|Distances| predstavlja klasu za izračunavanje rastojanja između dve muzičke numere. Računanje rastojanja, iako na prvi pogled trivijalno, može biti krucijalno i komplikovano u zavisnosti odabrane mere sličnosti. U okviru ovog rada je korišćeno Euklidsko rastojanje, a u budućim nadogradnjama sistema je planirana implementacija i drugih rastojanja. \verb|feature_group| komponenta koja je zadužena za izračunavanje statističkih parametara.Osim toga, ostavljena je mogućnost i da ova komponenta vrši i podelu atributa po grupama, što će takođe biti predmet buduće nadogradnje sistema. Umesto toga fokus je stavljen na izračunavanju atributa na celoj pesmi kao celini, efektivno umesto više grupa cela pesma je posmatrana kao jedna grupa. Dodatno postoji i \verb|Song| komponenta koja predstavlja kontejner za sve podatke koje karakterišu jednu pesmu. U okviru ove klase implementirano je i čitanje simboličkih podataka kao potencijalan izvor dodatnih atributa u daljim koracima razvoja sistema.

\begin{figure}[h]
\includegraphics[width=15cm, height=12cm]{playlist_class}
\centering
\caption{Klasni dijagram za Playlist, FeatureGroup, Song i Distances komponente}
\centering
\end{figure}

