
\chapter{Atributi}

Sličnost muzike je, kao što je ranije pomenuto, procena sličnosti muzičkih numera često zasnovana na subjektivnom doživljaju. Atributi kojima se opisuje muzička numera se mogu podeliti u tri osnovne kategorije:

\begin{itemize}
\item Muzički atributi niskog nivoa apstrakcije
\item Muzički atributi srednjeg nivoa apstrakcije
\item Muzički atributi visokog nivoa apstrakcije
\end{itemize}

Atributi srednjeg nivoa apstrakcije su uvedeni da bi premostili semantički raskorak između atributa niskog i visokog nivoa. Fokus ovog rada je na atributima niskog nivoa i njihovo korišćenje za formiranje mere sličnosti. Uprkos tome formulisaće se i osnova za atribute srednjeg nivoa apstrakcije.

\section{Atributi niskog nivoa apstrakcije}

Atributi niskog nivoa se direktno računaju na osnovu audio signala u vremenskom i/ili frekvencijskog domena. Obradiće se najkorišćeniji atributi niskog nivoa apstrakcije. Svi atributi se računaju na nivou okvira stoga su svi trenutni, to jest svaki zauzima najmanji mogući vremenski interval koji čovek može da prepozna. Iako se svi atributi računaju na nivou okvira, moguće je sumirati sve vrednosti određenog atributa na nivou jedne muzičke numere. Ovo se može uraditi računajući  statističke ocene kao što su medijana, srednja vrednost, varijansa ili maksimum. Kasnije će biti reči o korišćenju statističkih ocena. Postoje i drugi načini sumiranja vrednosti, jedan od njih je modelovanje distribucije pojedinačnog atributa pomoću Gausovog modela mešavine (eng. \textit{GMM - Gaussian mixture model})\cite{13}. Prilikom diskusije muzičkih atributa niskog nivoa koristićemo terminologiju navedenu u tabeli 3.1.

\begin{table}
	\begin{tabular}{c|c}
		\hline \hline
		Termin & Opis termina \\
		\hline \hline
		$s(n)$ & Amplituda k-tog uzorka \\
		\hline
		$m_t(k)$ & Vrednost signala u frekvencijskom domenu okvira $t$ u frekvencijskom opsegu $k$ \\
		\hline
		$N$ & Dužina okvira to jest, broj uzoraka u pojedinačnom okviru \\
		\hline
		$K$ & Broj frekvencijskih kanti tj. broj najvećeg frekvencijskog opsega \\
		\hline
	\end{tabular}
	\caption{Tabela termina korišćena prilikom diskusiji o atributima niskog nivoa apstrakcije}
\end{table}

Muzički atributi niskog nivoa apstrakcije se izračunavaju direktno iz sadržaja muzičke datoteke. Glavna podela ovakvih atributa je na atribute iz vremenskog domena i na atribute iz frekvencijskog domena. Atributi iz vremenskog domena se računaju na osnovu uzoraka tj. frekvencijskih amplituda, dok se atributi frekvencijskog domena računaju na osnovu spektrograma \cite{21}.

\subsection{Atributi iz vremenskog domena}
\subsubsection{Broj promena znaka amplitude}
Broj promena znaka amplitude (eng. \textit{ZCR - Zero Crossing Rate}) predstavlja  broj puta koliko signal pređe iz negativne u pozitivnu vrednost frekvencije i obratno. Može se koristiti kao primitivna mera detekcije visine tona. Vrlo uspešno se koristi u klasifikaciji perkusivnih zvukova i u detekciji govora.

\begin{figure}[h]
\includegraphics[width=8cm, height=5cm]{zcr}
\centering
\caption{Prikaz dijagrama za frekvenciju prelaska nule. Prvi dijagram je audio signal, drugi dijagram predstavlja dijagram frekvencije prelaska nule za dat audio signal.}
\centering
\end{figure}

Audio signal koji ne sadrži govor (instrumentalna muzika ili periferni zvuk) broj promena znaka amplitude teži ka visokim vrednostima, dok kod audio signala gde je prisutan govor, obično ima dosta manju vrednost. Zbog ove osobine se koristi kao primitivna mera za određivanje delova signala gde ima, odnosno nema, govora. Iako postoji mnogo stabilniji i tačniji algoritmi \cite{25} \cite{26} za određivanje postojanja govora ili detekcije visine zvuka, zbog relativno lakog izračunavanja a dovoljno dobre aproksimacije pomenutih osobina koristićemo broj promena znaka amplitude. Broj promena znaka amplitude može da služi i kao primitivni indikator koliko smetnji postoji u analiziranom signalu. Visoka vrednost obično znači da su smetnje prisutne u velikoj meri u analiziranom signalu. Izračunavanje broja promena znaka amplitude predstavljeno je u jednačini 3.1. Prilikom formiranja podataka treba obratiti pažnju na tip uzorka i skup odnosno interval vrednosti pomoću kojih se kodira digitalni signal koji analiziramo. Konkretni problem sa tipom uzoraka je što uzorak može da bude neoznačenog tipa, to jest može da uzima samo pozitivne vrednosti. U ovom slučaju se umesto nule bira prag, obično na sredini intervala.

\begin{equation}
ZCR = \frac {1}{2} \cdot \sum_{n = 0}^{N-1} |sgn(s(n)) - sgn(s(k+1))|
\end{equation}

\subsubsection{Autokorelacija prvog reda}

Autokorelacija prvog reda (eng. \textit{First Order Autocorrelation}) je mera koliko se signal poklapa sa pomerenom verzijom samog sebe u vremenu, odnosno prosečna vrednost odstupanja svaka dva susedna uzorka u signalu. Kao i frekvencija prelaska nule, može da se iskoristi da se identifikuju nagle promene intenziteta muzike tj. delove muzičke numere gde je pojačan intezitet muzičkih instrumenata i/ili vokala, osim toga veoma je korisna kao primitivna mera grubosti boje zvuka \footnote{http://www.musanim.com/wavalign/foote.pdf}. 

\begin{figure}[h]
\includegraphics[width=8cm, height=5cm]{foa}
\centering
\caption{Prikaz dijagrama autokorelacije prvog reda na primeru snimka otkucaja srca}
\centering
\end{figure}

Autokorelacija se često koristi prilikom određivanja mere sličnosti dva audio signala, pogotovo zbog osobine da može da aproksimira razliku postojećeg sa istim signalom ali različitom fazom. Autokorelaciju prvog reda izračunavamo pomoću formule prikazane jednačinom 3.2. 

\begin{equation}
FOA = \frac{1}{N} \cdot \sum_{n=1}^{N-1} s(n) \cdot s(n-1) 
\end{equation}

\subsubsection{Maksimalna vrednost amplitude}

Maksimalna vrednost amplitude (eng. \textit{Amplitude Envelope - AE}), kao što joj ime kaže, je maksimalna vrednost uzorka u okviru. Stoga se, prirodno, računa u vremenskom domenu. Formalna definicija je data u formuli (3.3)

\begin{equation}
AE = \max{\{s(n):n\in{0,1,...,N-1}\}}
\end{equation}

Maskimalna vrednost amplitude je veoma prost atribut koji se vezuje za muzički otkucaj (eng. \textit{Music Beat}). Muzički otkucaj je osnovni deo takta, odnosno svaki takt se sastoji iz određenog broja otkucaja. 


\begin{figure}[h]
\includegraphics[width=9cm, height=3cm]{envelope}
\centering
\caption{Uprošćeni prikaz maksimalne vrednosti amplitude u odnosu na audio signal.}
\centering
\end{figure}

Zbog svoje prirode je veoma podložan uticaju ekstremnih vrednosti. Ekstremna vrednost je vrednost signala koja u velikoj meri odudara od prosečne vrednosti, na primer, ako nam je od 100 uzoraka, 98 uzoraka u intervalu $[a,b]$ dok su poslednja dva uzorka iz intervala $[2\cdot b, 6 \cdot b]$ poslednja dva uzorka se mogu okarakterisati kao ekstremne vrednosti. 

\subsubsection{Srednjekvadratna energija}

Srednjekvadratna energija (eng. \textit{Root-Mean-Square Energy} ili \textit{RMS energy}) je još jedan atribut iz domena vremena. U literaturi se često naziva i RMS energija kao i RMS nivo ili RMS jačina. RMS potiče kao skraćenica engleskog naziva ovog atributa. Vezuje se za intezitet prepoznatog zvuka. Računa se prema formuli (3.4)

\begin{equation}
RMSEnergy = \sqrt{\frac{1}{N} \cdot \sum_{n=0}^{N-1}s(n)^2}
\end{equation}

Može se koristiti za određivanje nivoa glasnoće muzičke numere i kao indikator novih događaja u audio segmentu \cite{14}. Srednjekvadratna energija, kao atribut, je veoma slična maksimalnoj vrednosti amplituda, ali je manje osetljiva na ekstremne vrednosti, koje se često tumače kao šum. 

\begin{figure}[h]
\includegraphics[width=9cm, height=7cm]{rms}
\centering
\caption{Prikaz srednjekvadratne energije, po okvirima, za različite muzičke žanrove}
\centering
\end{figure}


Srednjekvadratna energija se može koristiti i za određivanje osobina muzičke numere vezanih za muzički otkucaj. Zbog svoje povezanosti sa muzičkim otkucajem, ritmom i glasnoćom muzikičke numere, neretko se ovaj atribut koristi i u aproksimaciji emocije koju muzička numera sa sobom nosi.

\subsection{Atributi iz frekvencijskog domena}
\subsubsection{Spektralni centroid}
Spektralni centroid (eng. Spectral Centroid) predstavlja epicentar magnitude spektra, odnosno frekvenciju gde je energija najviše koncentrisana. Ovaj atribut se najviše koristi kao mera svetline muzike, stoga je direktno vezana sa bojom zvuka \cite{15}\cite{15-1}. Boja zvuka je karakteristična za svaku vrstu zvuka. Isti ton može zvučati drugačije u zavisnosti od izvora zvuka. Boju određuje broj harmonika, odnosno njihovih amplituda i frekvencija. Parni harmonici daju toplinu i mekoću zvuku, dok neparni daju hladnoću i oštrinu \cite{27}. Spektralni centroid je veoma osetljiv na frekvencijske filtere, pogotovo na one koji ''razblažuju'' visoke frekvencije. Razlog ovome je što visoke frekvencije nose veću težinu prilikom računanja vrednosti spektralnog centroida. Ovaj problem posebno dolazi do izražaja kada je frekvencija uzimanja uzoraka mala. Posmatrajmo slučaj kada se ista numera posmatra u dve različite frekvencije uzimanja uzoraka, 44KHz i 11KHz. Na osnovu Nyquist-Shannon-ove teoreme, smanjenje frekvencije uzoraka će odseći ili proizvesti distorziju visokih frekvencije viših od polovine frekvencije uzimanja uzoraka. Prema tome, vrednost spektralnog centroida će se promeniti shodno izmeni i uticaju na amplitude frekvencije uzoraka. Formula za dobijanje vrednosti spektralnog centroida:
\begin{equation}
SC = \frac{ \sum_{k=1}^{K} m_t(k)^2 \cdot k }{ \sum_{k=1}^{K} m_t(k)^2  }
\end{equation}

Na slici 3.5 prikazane su vrednosti spektralnog centroida za prvih desetak sekundi pesama koje pripadaju potpuno različitim žanrovima. Da bi primer bio što ilustrativniji, korišćena je jedna pesma koja pripada tzv. novokomponovanom stilu (Aca Lukas - Kafana na Balkanu) i jedna pesma "hard rok" stila (ACDC - Highway to hell). Podaci su normalizovani u odnosu na sebe na interval [-1,1] radi lepšeg prikaza.

\begin{figure}[h]
\includegraphics[width=12cm, height=7cm]{spectral_centroid}
\centering
\caption{Vrednosti spektralnog centroida Kafana na Balkanu - Aca Lukas (plavo) i Highway to hell - ACDC (crveno)}
\centering
\end{figure}




\subsubsection{Spektralni raspon}
Spektralni raspon (eng. Spectral Spread), takođe poznat u literaturi i kao Spektralni protok (eng. \textit{Spectral Bandwith}) je atribut izveden iz spektralnog centroida. Spektralni protok predstavlja raspon spektra interesantnih delova signala, odnosno delova oko centroida. Može se interpretirati i kao varijansa srednje frekvencije signala. Defincija je data formulom 3.6. Prosečna vrednost spektralnog raspona muzičke numere se može koristiti kao opisna ocena zapažene boje zvuka \cite{16}.

\begin{equation}
SS = \frac{ \sum_{k=1}^{K} |k - SC | \cdot m_t(k) }{ \sum_{k=1}^{K} m_t(k)  }
\end{equation}

\subsubsection{Glatkoća zvuka}
Glatkoća zvuka (eng. \textit{Smoothness}) predstavlja ocenu koliko je zvuk ''ravan'' ili preciznije predstavlja ocenu meru promene zvuka u spektrumu \cite{17}. To je mera koliko je signal rasprostranjen po spektrumu. Beli šum (eng. \textit{white noise}), signal koji ima istu snagu na svim frekvencijama, će imati glatkoću oko 1, dok sinusoidni talas će imati glatkoću 0, pošto ima jedan vrh u spektrumu \cite{18}. 

\begin{figure}[h]
\includegraphics[width=12cm, height=7cm]{spectral_smoothness}
\centering
\caption{Glatkoća zvuka u prvih 10ak sekundi pesme Highway to hell. Podaci su normalizovani u odnosu na sebe na interval [-1,1]}
\centering
\end{figure}

Koristeći se ovim osobinama glatkoća zvuka nam daje dobru aproksimaciju buke (šuma) kao i ocenu harmoničnosti signala. Glatkoća se računa pomoću formule 3.7. Osim pomenutih primena, glatkoća zvuka se takođe koristi dosta i u prepoznavanju instrumenata. Zbog svoje karakterističnosti da reaguje na vrhove signala, može se modelovati sistem koji na osnovu ovog parametra može prepoznati da li određeni instrument učestvuje u numeri. \cite{19}
\begin{equation}
SpectralSmoothness = \sum_{k=1}^{K}20 \cdot \log_e m(k) - \frac{20 \cdot \log_e m(k-1) + 20 \cdot \log_e m(k) + 20 \cdot \log_e m(k+1)}{3}
\end{equation}

\subsubsection{Spektralna asimetrija}

Spektralna asimetrija (eng. \textit{Spectral Dissymetry}) ili iskrivljenost signala je mera koliko je spektrum iskrivljen u odnosu na spektralni centroid i shodno tome, koliko teži niskim odnosno visokim frekvencijama. Zajedno sa glatkoćom zvuka i rasponom predstavlja još jedan atribut koji nam pruža informaciju o obliku spektra. U ovom slučaju, mera je bazirana na harmoničnosti zvuka oko spektralnog centroida. Spektralna asimetrija se dobija korišćenjem formula 3.8.

\begin{equation}
SpectralAsymetry = \sqrt{ \frac{\sum_{k = 1}^{K} m(k) \cdot (\frac{D_{rate}}{2} - SC)}{\sum_{k=1}^{K}m(k)} }
\end{equation}

$D_{rate}$ predstavlja frekvenciju uzimanja uzoraka referentnog signala, u ovom slučaju, kao što je već napomenuto, vrednost frekvencije uzimanja uzoraka je 44100Hz, što direktno znači da je $D_{rate} = 22050$. Na slici 3.7, kao kod slučaja spektralnog centroida, su prikazane normalizovane vrednosti spektralne asimetrije za dve muzičke numere. Jedna pripada novokomponovanom žanru, dok druga pripada hard rok žanru.

\begin{figure}[h]
\includegraphics[width=12cm, height=7cm]{spectral_centroid}
\centering
\caption{Vrednosti spektralne asimetrije  Kafana na Balkanu - Aca Lukas (plavo) i Highway to hell - ACDC (crveno).}
\centering
\end{figure}

\subsection{Implementacioni detalji}

Računanje pomenutog skupa atributa se dešava u komponenti \verb|WindowFeatureExtractor|, čija se implementacija nalazi u okviru datoteka \verb|WindowFeatureExtractor.hh/.cc|. Računanje skupa atributa je za sada implementirano preko skupa metoda pomenute komponente, odnosno klase. U daljem radu i proširenju sistema treba uzeti u obzir i moguć način spoljašnjeg definisanja računanja atributa, u vidu dodataka (eng. \textit{plug-in}), ili pomoću nekog definisanog pseudo-računarskog jezika koji će se izvršavati dinamički.

\begin{figure}[h]
\includegraphics[width=10cm, height=8cm]{win_feature_extractor}
\centering
\caption{Klasni dijagram implementacionih detalja za računanje atributa. Samo jedna komponenta je odgovorna za ovo stoga samo jedna klasa}
\centering
\end{figure}

\subsection{Atributi srednjeg nivoa}

U prethodnom tekstu su definisani nivoi atributa koji se koriste za aproksimaciju muzičkih numera. Fokus ovog rada je na atributima niskog nivoa, ali ćemo zbog kompletnosti, ukratko objasniti i atribute srednjeg nivoa. Atributi niskog nivoa su prvenstveno statističke ocene nad signalom i njegovom frekvencijskom i/ili vremenskom obliku. Sami za sebe ne nose nikakvu značajnu informaciju koju bi čovek mogao da interpretira i na osnovu njih izvuče smislene zaključke. Na primer, na osnovu samih brojeva ne može se zaključiti da li je pesma bržeg ili sporijeg tempa, da li je reč o baladi ili pesmi veselije melodije itd. Takvi opisi se nazivaju atributi visokog nivoa, odnosno atributi koje čovek koristi da bi opisao pesmu. Da bi se premostio jaz između atributa niskog nivoa i visokog nivoa uvode se atributi srednjeg nivoa koji kombinuju vrednosti jednog ili više atributa niskog nivoa da bi se bliže definisali atributi visokog nivoa. 

U okviru atributa srednjeg nivoa se često koriste Cepstralni koeficijenti Melove frekvencije. Cepstrum predstavlja rezultat dobijen primenom inverzne furijeove transformacije nad logaritmom spektruma. Ime Cepstrum potiče od permutacije prva četiri slova engleske verzije termina spektrum. Cepstrum se dosta koristi u analizi i prepoznavanju govora, a i u oblasti istraživanja muzičkih podataka \cite{28}. Mel je izvedena jedinica za frekvenciju koja se definiše preko formule 

\begin{equation}
mel = 1127 \cdot log(1 + \frac{f}{700})
\end{equation}

gde je $f$ predstavlja vrednost frekvencije u hercima.

Takođe se, za svrhe definisanja atributa srednjeg nivoa se izvodi još jedna jedinica za frekvenciju, Bark.
\begin{equation}
bark = (\frac{26.81 \cdot f}{1960 + f}) - 0.53
\end{equation}

gde $f$ takođe predstavlja vrednost frekvencije u hercima \cite{21}.

Izvedene jedinice frekvencije se koriste radi bližeg određivanja vrednosti frekvencije ljudskom shvatanju muzike. Pomenutim formulama dobijamo preslikavanje frekvencija tako da se više obraća pažnja na intervale odnosno vrednosti frekvencija na koje čovek više reaguje. Razlog izvođenja ovih jedinica leži u psihoakustici, oblasti koja se bavi ljudskom percepcijom zvuka, odnosno načinu na koji čovek interpretira neke od osnovnih zvučnih karakteristika, glasnoću, frekvenciju, intezitet i slično.

Osim pomenutih izvedenih jedinica, definisanje atributa srednjih nivoa se odnosi i na kombinaciju dobijenih atributa. Ovde se prvenstveno misli na računanje atributa po okviru, pa se rezultati kasnije kombinuju pomoću Gausovih modela i vektorizacije radi daljeg računanja. Vektorizacija podrazumeva grupisanje podataka u vektore. Korišćenjem vektora umesto pojedinačnih promenljivih dosta uprošćava potrebne korake pri računu svodeći na vektorski račun. Još jedna bitna tehnika je formiranje takozvanih blok-okvira. Blok-okvir je grupisanje određenog broja okvira u posebnu grupu koja će kasnije tokom izračunavanja sličnosti biti uzeta u obzir prilikom računanja, to direktno znači da će svaki blok učestovati u izračunavanju aproksimacije sličnosti. Ovaka pristup je vrlo efikasan, iako računski prilično zahtevan. Efikasnost se prvenstveno ogleda u sposobnosti da se računa sličnost za pojedinačne fragmente muzičke numere a ne cele muzičke numere u potpunosti. Ovde se koriste tehnike logaritmski fluktuirajućih šablona, spektralnih šablona i slično.
Detaljno objašnjenje tehnika i pojmova atributa srednjeg nivoa izlazi iz obima ovog rada. Više informacija o ovim atributima može se pronaći u \cite{21}.
                                                                                                                                                                                                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                                                                                                                                                                                                      

