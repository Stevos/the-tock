\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Uvod}{1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Struktura rada}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Sli\IeC {\v c}nost i nala\IeC {\v z}enje muzi\IeC {\v c}kih numera}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Istra\IeC {\v z}ivanje podataka u muzici}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Sli\IeC {\v c}nost muzike}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.1}Uticaj emocija na muzi\IeC {\v c}ku sli\IeC {\v c}nost}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.2}Ra\IeC {\v c}unski faktori muzi\IeC {\v c}ke sli\IeC {\v c}nosti}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.3}Semanti\IeC {\v c}ki raskorak}{8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Osnovne metode obrade audio signala}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Prozorske funkcije i okviri}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Implementacioni detalji}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Furijeova transformacija}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Implementacioni detalji}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Biblioteke}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Atributi}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Atributi niskog nivoa apstrakcije}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Atributi iz vremenskog domena}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.1}Broj promena znaka amplitude}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.2}Autokorelacija prvog reda}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.3}Maksimalna vrednost amplitude}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.4}Srednjekvadratna energija}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Atributi iz frekvencijskog domena}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.1}Spektralni centroid}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.2}Spektralni raspon}{23}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.3}Glatko\IeC {\'c}a zvuka}{24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.4}Spektralna asimetrija}{24}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Implementacioni detalji}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.4}Atributi srednjeg nivoa}{25}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Mera sli\IeC {\v c}nosti}{28}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Statisti\IeC {\v c}ki parametri}{29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Mera sli\IeC {\v c}nosti}{30}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Test skup}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Predstavljanje rezultata}{32}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Normalizacija statisti\IeC {\v c}kih ocena atributa}{35}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Ocena mere sli\IeC {\v c}nosti}{38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Implementacioni detalji}{39}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Zaklju\IeC {\v c}ak}{41}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Prate\IeC {\'c}e informacije}{42}
