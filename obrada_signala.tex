\chapter{Osnovne metode obrade audio signala}


Aproksimacija sličnosti dve muzičke numere na osnovu prirodnih karakteristika zvuka se primarno odnosi na izvlačenje skupa atributa iz muzičkog sadržaja. Muzički sadržaj se reprezentuje datotekama koje sadrže digitalni zapis zvuka. Izvođenje atributa je glavni i centralni proces u okviru ovog rada. Proces izvođenja atributa, kojim se bave komponente poznatije kao ''ekstraktori atributa'', ima za cilj da na osnovu audio signala formira skup atributa. Pre svega toga, sirov audio signal mora da prođe kroz niz operacija i obrada da bi se dobili podaci optimizovani i spremni za nalaženje atributa. U okviru ovog poglavlja će biti razmatrani osnovni aspekti i elementarne tehnike obrade audio signala. Pored teorijske osnove, biće i reči o implementacionim detaljima u okviru testnog sistema koji će biti razvijen radi demonstracije prikazanih stvari u ovom radu.

Zvuk se definiše kao mehanički talas frekvencija između 16 i 20000 Herca, odnosno zvuk koji čovek može da čuje. Postoje i infrazvuk i ultrazvuk, odnosno zvuk ispod granice od 16Hz i iznad granice od 20KHz respektivno. Akustični muzički instrumenti podstiču vazduh da oscilira na različitim frekvencijama čime proizvode zvuk. Zvuk proizveden od strane akustičnih muzičkih instrumenata je analogne prirode i može se predstaviti netrivijalnom matematičkom funkcijom, koja za određen trenutak u vremenu rezultuje frekvencijskom amplitudom. Uređaji za snimanje imaju ograničenu vremensku rezoluciju, te stoga ne mogu u snimiti kontinualni signal. Problem se prevazilazi uzimanjem uzoraka signala na određenom vremenskom intervalu. Uzimanje uzoraka je proces merenja amplitude frekvencije u datom trenutku. Merenje se vrši pomoću specijalizovane membrane koja detektuje frekvenciju oscilacije (mikrofona\footnote{Uređaji kojim se vrši ovo merenje su uglavnom mikrofoni. Princip po kome funkcioniše mikrofon je veoma sličan principu reprodukcije zvuka preko zvučnika, zapravo mikrofon predstavlja mali zvučnik i sposoban je da reprodukuje zvuk dosta manje jačine nego zvučnik. Posledica toga je manja membrana, zbog osetljivosti na spoljašnje vazdušne vibracije.}). Nakon merenja frekvencije, dobijena vrednost se normalizuje na unapred zadat interval. U ovom slučaju normalizacija podrazumeva dva koraka, prvi korak je ignorisanje vrednosti van intervala zvuka [16Hz,20000Hz], odnosno njihovo zaokruživanje na najbližu granicu intervala. Drugi korak predstavlja preslikavanje vrednosti iz intervala zvuka u interval zadat prilikom digitalizacije (npr. [-1,1]).
Opisan proces uzimanja uzoraka se zove i kvantizacija signala. Proces kvantizacije stvara i grešku kvantizacije. Greška kvantizacije nastaje kada se vrednost kontinualnog signala i mapirana diskretna vrednost razlikuju. 

\begin{figure}[h]
\includegraphics[width=8cm, height=5cm]{quantization_error}
\centering
\caption{Greška kvantizacije. Analogni signal je predstavljen crvenom bojom, dok je plavom predstavljen odgovarajući digitalni signal.}
\centering
\end{figure}

Na slici se lako uočava greška kvantizacije, razlika u vrednostima između plave i crvene linije. Iz čega zaključujemo da je rezolucija, ili diskretni interval iz kog uzimamo vrednosti prilikom analogno-digitalne konverzije, važan za kvalitet rezultata. Često se rezolucija poistovećuje sa memorijom dostupnom za zapis vrednosti, odnosno brojem bitova u koji možemo da zapišemo jedan uzet uzorak. 
Kod analogno-digitalne konverzije pored rezolucije, frekvencija uzimanja uzoraka predstavlja još jedan izuzetno bitan faktor. Nyquist-Shannon-ova teorema kaže da frekvencija uzimanja uzoraka signala mora da bude najmanje $2 \cdot f$ da bi se zapisale frekvencije do $f$ Herca \cite{11}. Ako je frekvencija suviše mala postaje nemoguće zabeležiti pun spektar frekvencija koje ljudsko uho može da prepozna.

\section{Prozorske funkcije i okviri}

Posmatrajući digitalni signal kao niz uzoraka, svaki uzorak ponaosob predstavlja izuzetni mali deo vremena. Kod najrasprostranjenije frekvencije uzimanja uzoraka od 44KHz, uzorak pokriva oko ~23 mikrosekunde. Ovaj interval je suviše kratak da bi ga ljudsko uho registrovalo kao zvuk. Ljudsko uho može da prepozna dužinu zvuka od najmanje ~10 milisekundi. Izračunavanje atributa se vrši na skupu uzoraka, koji kad se reprodukuje proizvodi dovoljno dug zvuk da ga ljudsko uho registruje. Izračunavanje atributa na celoj muzičkoj numeri u celosti, iako moguća tehnika, neće pružiti mnogo informacija. Osnovni razlog za ovo je što se na taj način ne dobija nikakva informacija o raspodeli atributa u toku trajanja pesme, već samo vrednost atributa za celu pesmu. Stoga se vrši izračunavanje atributa nad manjim skupovima uzoraka, koji se nazivaju okviri (eng. \textit{frame}). Dužina okvira se definiše kao stepen dvojke u intervalu $2^{8}$ (256) - $2^{13}$(8192). Moguće je uzimanje dužine i van pomenutog intervala, ali u literaturi i današnjim sistemima je to vrlo retko. Dužina okvira mora zadovoljiti uslov da je jednaka nekom stepenu dvojke zbog kasnije primene brze Furijeove transformacije. Iako stepen dvojke nije obavezan uslov za Furijeovu transformaciju, dosta doprinosi performansama izračunavanja Furijeove transformacije i često je obavezan uslov za korišćenje već implementiranih transformacija u okviru dostupnih biblioteka.
 
Okviri predstavljaju ulaz za komponente koje vrše izračunavanje atributa koji rade sa signalom u vremenskom domenu. Za atribute iz frekvencijskog domena, primenjuje se Furijeova transformacija na okvir pre izračunavanja. Pre primene Furijeove transformacije nad okvirom obavezno je uraditi neki vid normalizacije nad okvirima. Normalizacija se vrši primenom unapred određene prozorske funkcije nad okvirom. Uobičajen izbor je takozvana Hann-ova prozorska funkcija, nazvana po austrijskom meteorologičaru Julius-u von Hann-u. Prozorska funkcija je definisana u jednačini 2.1, gde je $N$ dužina okvira i $n = 1 ... N$.

\begin{equation}
w(n) = 0.5 \cdot \left(1 - \cos\left(\frac{2\cdot\pi\cdot n}{N-1}\right)\right)
\end{equation}

Množenjem svakog uzorka u okviru sa odgovarajućom vrednošću Hannove funkcije dobijamo periodični signal. Ovo je izuzetno bitno zato što Furijeova transformacija stvara artifakte u rezultujućem spektrumu za funkcije koje nisu periodične. Ovaj problem se naziva \textit{spektralno curenje}. Proces normalizacije okvira sa prozorskom funkcijom je prikazan i grafički na slici 2.2.

\begin{figure}[h]
\includegraphics[width=5cm, height=8cm]{windowing_hann}
\centering
\caption{Prikaz primene Hannove prozorske funkcije na signal}
\centering
\end{figure}

Gornji dijagram prikazuje ulazni signal, odnosno okvir od 128 uzoraka. Srednji dijagram prikazuje izgled Hannove funkcije nad istim brojem uzoraka. Najniži dijagram prikazuje izgled izlaznog signala u okviru posle primene normalizacije prozorskom funkcijom. 
Kao što se vidi na slici, primena normalizacije prozorskom funkcijom potiskuje uzorke sa oba kraja okvira. Da bi se izbeglo gubljenje informacija zbog pomenutog potiskivanja, obično se uzastopni okviri preklapaju. Dužina preklapanje se naziva i hop dužina (eng. \textit{hop size}). Ilustrovano na primeru, uzećemo dužinu okvira 256 i hop dužinu od 128 uzoraka, prvi okvir uzima vrednosti uzoraka na pozicijama 1-256, drugi okvir uzima uzorke na pozicijama 129-385, treći okvir uzorke na pozicijama 257-513 i sve tako redom. Na prikazanom primeru uzastopni uzorci se preklapaju u proporciji od 50\% što je čest izbor u procesiranju muzičkog signala \cite{21}.
\subsection{Implementacioni detalji}
U okviru testnog sistema koji će biti razvijen, pružene su opcije biranja prozorske funkcije za normalizaciju. Kako je sistem predviđen da demonstrira navedene koncepte u ovom radu, pored toga dizajniran je sa namerom da čitaocu pruži mogućnost daljeg eksperimentisanja sa pomenutim konceptima. Korisniku je omogućeno da učita sopstvenu prozorsku funkciju za normalizaciju kao i sledeće podrazumevane prozorske funkcije, odakle korisnik može da izabere koju želi da primeni. Svi koncepti se mogu konfigurisati u okviru \verb|config.json| datoteke. Odabir prozorske funkcije za normalizaciju se vrši setovanjem parametra \verb|NormFunc| kome se prosleđuje ili putanja do datoteke ili ime neke od već implementiranih prozorskih funkcija za normalizaciju.
Već implementirane prozorske funkcije za normalizaciju, pored Hann-ove su:
\begin{itemize}
\item Bartletova prozorska funkcija
\begin{figure}[h]
\includegraphics[width=8cm, height=4cm]{bartlet}
\centering
\caption{Bartletova ili trougaona prozorska funkcija \cite{24}}
\centering
\end{figure}
\begin{equation}
w(n) = 1 - \left| \frac{n-\frac{N-1}{2}}{\frac{N}{2}}\right|
\end{equation}

\item Welch-ova prozorska funkcija
\begin{figure}[h]
\includegraphics[width=8cm, height=4cm]{welch}
\centering
\caption{Welchova prozorska funkcija \cite{24}}
\centering
\end{figure}
\begin{equation}
w(n) = 1 - \left(\frac{n - \frac{N-1}{2}}{\frac{N-1}{2}}\right)^2
\end{equation}

\item Sinusna prozorska funkcija
\begin{figure}[h]
\includegraphics[width=8cm, height=4cm]{sine}
\centering
\caption{sinusna prozorska funkcija \cite{24}}
\centering
\end{figure}
\begin{equation}
w(n) = \sin\left(\frac{\pi \cdot n}{N-1}\right)
\end{equation}

\item Pravougaona prozorska funkcija 
\begin{figure}[h]
\includegraphics[width=8cm, height=4cm]{rectangular}
\centering
\caption{Pravougaona prozorska funkcija \cite{24}}
\centering
\end{figure}
\begin{equation}
w(n) = 1
\end{equation}

\item Hammingova prozorska funkcija
\begin{figure}[h]
\includegraphics[width=8cm, height=4cm]{hamming}
\centering
\caption{Hammingova prozorska funkcija \cite{24}}
\centering
\end{figure}
\begin{equation}
w(n) = \alpha - \beta \cdot \cos\left(\frac{2 \cdot \pi \cdot n}{N-1}\right)
\end{equation}
sa $\alpha = 0.54$, $\beta = 1 - \alpha = 0.46$

\end{itemize}

Uz svaku prozorsku funkciju je data formula koja je određuje, kao i slika koja predstavlja prozorsku funkciju i rezultujuć izlaz Furijeove transformacije za konstantan ulazni signal. 

\section{Furijeova transformacija}

Furijeova transformacija je nazvana prema francuskom matematičaru i fizičaru Jean Baptiste Joseph Fourier. Predstavlja transformaciju signala iz vremenskog u frekvencijski domen. Zasniva se na Furijeovoj teoremi koja kaže: "Svaka kontinualna i periodična funkcija može biti predstavljena sumom sinusnih i kosinusnih talasa koji osciliraju na različitim frekvencijama."
Furijeova transformacija rezultuje diskretnim skupom kompleksnih vrednosti, koje opisuju frekvencijski spektrum ulaznog signala. Pošto Furijeova transformacija predstavlja fundamentalni alat u obradi signala, razvijeno je nekoliko različitih pristupa i algoritama za njeno izračunavanje, kratko će biti opisani pristupi koji se tiču obrade audio signala.
Računski najmanje zahtevna varijanta Furijeove transformacije je tzv. brza Furijeova transformacija (eng. \textit{Fast Fourier Transformation - FFT}, koja može biti korišćena ako je ulazni signal diskretan, što jeste slučaj kod digitalnih audio signala. Stoga FFT je diskretna Furijeova transformacija (DFT) definisana jednačinom 2.7, gde je $k$ celobrojna vrednost na intervalu od 0 do $N-1$, $N$ je dužina okvira odnosno broj uzoraka u okviru i $x_n$ je $n^{ti}$ uzorak iz okvira, odnosno odgovarajuća frekvencijska amplituda.

\begin{equation}
X_k = \sum_{n=0}^{N-1} x_n \cdot e^{-\frac{2 \cdot \pi \cdot i}{N} \cdot n \cdot k}
\end{equation}

Svaka izlazna vrednost $X_k$ diskretne furijeove transformacije je kompleksan broj, čiji realan deo $Re(X_m)$ predstavlja značaj kosinusnih talasa i imaginarni deo $Im(X_m)$ predstavlja značaj sinusnih talasa. Odakle možemo da dobijemo informacije o vrednosti i fazi date frekvencije. 

\begin{equation}
|X_n| = \sqrt{Re(X_n)^2 + Im(X_n)^2}
\end{equation}

Jednačina 2.8 nam daje način za određivanje amplitude kombinovanih sinusnih i kosinusnih talasa. 

\begin{equation}
\phi(X_n) = tg^{-1}\frac{Im(X_n)}{Re(X_n)}
\end{equation}

Jednačinom 2.9 se definiše faza tj. relativna proporcija sinusa i kosinusa.
Izračunavanjem $X_n$ za različite frekvencije iz vremenskog domena ulaznog signala $x_n$ direktno iz jednačine 2.7, dobija se vremenska složenost $O(n^2)$, odnosno za svaki od $N$ izlaza sumira se $N$ elemenata. Da bi se značajno smanjila vremenska složenost, Cooley i Tukey 1965. godine predlažu algoritam koji se izvršava u $O(N \cdot log N)$ složenosti \cite{12}. Kao početni zahtev je postavljen uslov da ulazni signal pripada diskretnom skupu kao i to da njihov algoritam zahteva da je $N$, odnosno dužina okvira, stepen dvojke. Pomenuti uslovi, obično nisu ograničavajući faktori u svakodnevnoj upotrebi pa je ova verzija algoritma (odnosno njene sitne modifikacije) danas i najrasprostranjenija u industriji.

\begin{figure}[h]
\includegraphics[width=10cm, height=7cm]{dft}
\centering
\caption{Prikaz ulaznog signala u vremenskom domenu i prikaz rezultata DFTa (frekvencijski domen)}
\centering
\end{figure}

\section{Implementacioni detalji}

Kao što je bilo prethodno reči o tome, u okviru ovog rada je razvijen sistem koji će demonstrirati sve pomenute koncepte. Koristimo reč sistem jer svaka komponenta u okviru aplikacije razvijana je sa mogućnošću da se koristi samostalno. U ovoj sekciji će biti opisane komponente i organizacija dela za obradu signala i pripremu za računanje atributa. Problem postojanja velikog broja formata audio zapisa unosi veliki nivo kompleksnosti, potrebno je podržati barem najčešće formate. Svaki format sa sobom nosi različite načine zapisa uzorka, nivoa kompresije, broja kanala i sličnog. Da bi bilo moguće raditi smislenu analizu i poređenje, prvi korak je svođenje na istu kategoriju, odnosno prevođenje u isti format sa istim brojem uzoraka, kanala i tipom uzorka. Komponente koje to rade su \verb|RawDataExtractor| i \verb|FFMpegResampler|. Komponentne u ''ispod haube'' koriste biblioteku FFMPEG prilikom rada sa formatima. Njihova primarna uloga je da za datu putanju audio datoteke pročitaju uzorke i formiraju okvire za kasniju obradu. Iako FFMPEG biblioteka podržava rad sa formatima, potrebno je prilagoditi ih za izvlačenje audio signala u odgovarajućem obliku, stoga su ove komponente razvijene. Prilikom čitanja uzoraka bilo je neophodno izvršiti njihovu modifikaciju i ponovno uzorkovanje radi ''svođenja na isto'' zbog kasnije ravnopravne analize i poređenja. \verb|FFMpegResampler| služi da svaki fajl svede na predefinisanu, istu za sve, frekvenciju uzimanja uzoraka. Pored toga se radi i pretvaranje višekanalnog zapisa u jednokanalni ili mono zapis. Postojanje više kanala, u zavisnosti od sadržaja, može da oteža ili olakša posao, međutim u ovom slučaju to znatno proširuje prostor pretrage i prostor parametara, što nije fokus ovog rada. Stoga se radi prebacivanje u jedan kanal koji će sadržati sve informacije sa svih postojećih kanala. Ovakav pristup predstavlja kompromis između kompleksnosti rezultujućeg sistema i izračunavanja. Za aproksimaciju mere sličnosti, prema ljudskom shvataju muzike, broj kanala ne predstavlja toliko bitan uslov. \verb|RawDataExtractor| za prosleđenu putanju obezbeđuje formiranu i napunjenu komponetnu \verb|WindowData| koja sadrži formirane okvire prema konfiguracionim parametrima. U trenutku pisanja komponente terminilogija je bila premutovana, pa se zapravo reč \verb|Window| odnosi na okvir. \verb|WindowData| je zapravo omotač oko vektora \verb|Window| komponente. Obe komponente služe kao kontejner za čuvanje podataka u željenom i odgovarajućem obliku. Konfiguracioni parametri vezani za dužinu okvira, frekvenciju uzimanja uzoraka i sličnog se nalaze u \verb|config.json| datoteci. Na slici 2.9 se nalazi dijagram klasa pomenutih komponenti, sa akcentom na javni interfejs i relacije između komponenti.

\begin{figure}[h]
\includegraphics[width=14cm, height=9.5cm]{raw_class_diagram}
\centering
\caption{Klasni dijagram za komponente vezane za obradu audio signala i čuvanje relevantnih podataka.}
\centering
\end{figure}

\subsection{Biblioteke}
Postoji veliki broj formata audio zapisa. Svaki podrazumeva različit nivo kompresije i različite načine zapisa podataka, bilo da li je do tipa zapisa uzorka (\verb|uint16_t, float, uint8_t|), broj kanala i slično. Kako to nije glavna tema ovog rada, za korišćenje, čitanje i neznatnu obradu ulaznih audio datoteka korišćenja je biblioteka FFMPEG (www.ffmpeg.org). FFMPEG je biblioteka otvorenog kôda za rad sa multimedijalnim datotekama. Sastoji se iz nekoliko manjih biblioteka za obradu multimedijalnog signala. Podržava rad sa audio i video materijalom i orijentisana je da objedini sve potrebne formate i kodeke u okviru jedne biblioteke. Primarni fokus biblioteke je okrenut ka konverziji i čitanju formata radi kasnije reprodukcije. Takođe je podržano osnovno modifikovanje multimedijalnog materijala. FFMPEG predstavlja industrijski standard kada je obrada multimedijalnog signala i danas je veoma rasprostranjena i stoji iza većine aplikacija koje se bave ovom tematikom. Zbog svoje stabilnosti i širine predstavlja logičan izbor za realizaciju ovog rada, osim toga pisana je u C-u. C interfejs se bez mnogo muke integriše i koristi u C++ kôdu, što je još jedan razlog za korišćenje biblioteke. Takođe, zbog svoje popularnosti postoji verzija biblioteke za većinu dostupnih platformi i operativnih sistema. U okviru ovog rada biblioteka je korišćena za čitanje i dekompresiju ulaznih audio datoteka i fft transformaciju ulaznog signala. Implementirana je i mogućnost čitanja dostupnih meta podataka, ali akcenat nije na korišćenju istih, već kao način za dobavljanje podataka za kasniji bolji estetski prikaz. 



